<?php 
session_start();
header('Content-type: application/json');
require_once("../required/functions.php");
require_once("../required/connbd.php");
require_once("alumno.php");

if(isset($_POST) && isset($_POST["codigo"]) && isset($_POST["accion"]) && $_POST["accion"] === "ALU"):
    
    $obj                = new Alumno();
    $obj->codigo        = $_POST["codigo"];
    $obj->nombre        = $_POST["nombre"];
    $obj->paterno       = $_POST["paterno"];
    $obj->materno       = $_POST["materno"];
    $obj->fNacimiento   = FechaBD($_POST["nacimiento"]);
    $obj->planes        = $_POST["planes"];
    $obj->rut           = $_POST["rut"];
    $obj->celular       = $_POST["celular"];
    $obj->email         = $_POST["email"];
    $obj->estado        = $_POST["estado"];

    $becado = 0;
    if(isset($_POST["becado"]))
        $becado = 1;

    $obj->becado = $becado;    
    #---pre($_POST, true);

    $obj->Save();

    $resp               = new stdClass();

    if($obj->ExisteUsuario()):
        $resp->resultado    = "OK";
    else:
        $resp->codigo       = $obj->codigo;
        $resp->resultado = "OK-N";
    endif;
    
    echo json_encode($resp);
endif;

if(isset($_POST) && isset($_POST["accion"]) && $_POST["accion"] == "USR" && $_POST["codigo"] > 0):
    #--- instancia el objeto
    $obj                = new Alumno();
    $obj->codigo        = $_POST["codigo"];
    $obj->nombre        = $_POST["nombre"];
    $obj->paterno       = $_POST["paterno"];
    $obj->materno       = $_POST["materno"];
    $obj->email         = $_POST["email"];

    #--- registra al alumno como un usuario del sistema
    $obj->GuardarUsuario();

    #--- envía correo al usuario con sus respectivas credenciales 
    
    $msj        = "Hola ".$obj->getNombreCompleto()."! <br><br>";
    $msj       .= "Bienvenido al gimnasio. Para ingresar al sistema debes ingresar las siguientes credenciales: <br><br>";
    $msj       .= "<b>Usuario:</b> ".$obj->nomusuario." <br>";
    $msj       .= "<b>Contraseña:</b> ".$obj->contrasena." <br><br>";
    $msj       .= "En el sistema podrás encontrar tus asistencias, ver tu progreso a medida que entrenas, etc.";

    #--- se envía correo al usuario
    if($obj->email != "")
        EnviarCorreo("Bienvenido a Salud y Rendimiento", $obj->email, $msj);
    
    #--- retorna exito de la operación
    $resp               = new stdClass();
    $resp->resultado    = "OK";
    
    echo json_encode($resp);
endif;

?>
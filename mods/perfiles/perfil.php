<?php   

class Perfil{
    public $id;
    public $nombre;
    public $estado;
    public $modulos;
    public $usuario;
    public $creacion;

    public function Get($id){
        $conn       = new connbd();
        $strconn    = $conn->connect();

        #--- limpia el id
        $id         = clear($id, $strconn);
        
        #--- trae la cabecera
        $sqlCabecera = "SELECT nombre FROM perfil WHERE id = ".$id;
        $resCabecera = $strconn->query($sqlCabecera) or die("Error get: ".mysqli_error($strconn));

        if($resCabecera->num_rows > 0):
            $row = $resCabecera->fetch_assoc();
            $this->nombre = $row["nombre"];
        endif;

        #--- trae los módulos asociados a este perfil
        $sqlMods = "SELECT id_modulo FROM perfil_modulos WHERE id_perfil = ".$id;
        $resMods = $strconn->query($sqlMods) or die ("Error get2: ". $resMods);
        $array   = array();
        if($resMods->num_rows > 0):
            while($row = $resMods->fetch_assoc()):
                $array[] = $row["id_modulo"];
            endwhile;
        endif;

        $this->modulos = $array;
        return $this;
    } 

    public function GetAll(){
        $conn       = new connbd();
        $strconn    = $conn->connect();
        $sql        = "SELECT id, nombre, estado, creacion, login FROM perfil ";
        $res        = $strconn->query($sql) or die ("Error getall: " . mysqli_error($strconn));
        
        $Gd_array   = array();
        if($res->num_rows > 0):
            while($row = $res->fetch_assoc()):
                $obj            = new stdClass();
                $obj->id        = $row["id"];
                $obj->nombre    = $row["nombre"];
                $obj->estado    = $row["estado"];
                $obj->creacion  = dt($row["creacion"]);
                $obj->usuario   = getUser($row["login"]);
                $Gd_array[]     = $obj;
            endwhile;
        endif;

        $strconn->close();
        return $Gd_array;
    }

    public function GetModulos(){
        $conn       = new connbd();
        $strconn    = $conn->connect();
        $sql        = "SELECT id, nombre FROM modulos ";
        $res        = $strconn->query($sql) or die ("Error get modulos: " . mysqli_error($strconn));
        
        $Gd_array   = array();
        if($res->num_rows > 0):
            while($row = $res->fetch_assoc()):
                $obj            = new stdClass();
                $obj->id        = $row["id"];
                $obj->nombre    = $row["nombre"];

                $Gd_array[]     = $obj;
            endwhile;
        endif;

        $strconn->close();
        return $Gd_array;
    }

    public function Save(){
        $conn       = new connbd();
        $strconn    = $conn->connect();

        if($this->Exists($this->id)):
            $sql        = "UPDATE perfil SET nombre = '".$this->nombre."' where id = ".$this->id; 
            $strconn->query($sql) or die("Error update 1:" . mysqli_error($strconn));
        else:
            $sql        = "INSERT INTO perfil (nombre, estado, login) VALUES ('";
            $sql       .= $this->nombre."', 'A', ".$this->usuario . ")";
            $res        = $strconn->query($sql) or die("Error insert" . mysqli_error($strconn) );
            $this->id   = $strconn->insert_id;
        endif;

        #--- asocia los módulos al perfil
        $this->AddModulos();

    }

    public function Exists($id){
        if($id == "")
            return false;

        $conn       = new connbd();
        $strconn    = $conn->connect();
        
        $sql        = "select count(nombre) as count from perfil where id = ".$id;
        $res        = $strconn->query($sql) or die("Error exists:" . mysqli_error($strconn));

        if($res->num_rows > 0):
            $row = $res->fetch_array();
            if($row["count"] > 0):
                return true;
            else:
                return false;
            endif;
        endif;
    }

    private function AddModulos(){
        $conn       = new connbd();
        $strconn    = $conn->connect();
        
        #--- elimina los modulos para actualizar la tabla
        $this->DeleteModulos();

        for($i = 0; $i < count($this->modulos); $i++):
            $sql  = "INSERT INTO perfil_modulos(id_perfil, id_modulo, estado)";
            $sql .= "values(".$this->id.", ".$this->modulos[$i].", 'A')";

            $strconn->query($sql);
        endfor;

        $strconn->close();
    }

    private function DeleteModulos(){
        $conn       = new connbd();
        $strconn    = $conn->connect();

        $sql        = "DELETE FROM perfil_modulos WHERE id_perfil = ".$this->id;
        $strconn->query($sql) or die("Error delete". mysqli_error($strconn));
        $strconn->close();
    }

    public function CambiarEstado(){
        $conn       = new connbd();
        $strconn    = $conn->connect();
        
        #--- limpia variables
        $estado     = clear($this->estado, $strconn);
        $id         = clear($this->id, $strconn);

        $sql        = "UPDATE perfil SET estado = '".$estado."' WHERE id = ".$id;
        $strconn->query($sql) or die ("Error cambiar estado: ".mysqli_error($strconn));
        $strconn->close();
    }
}

?>
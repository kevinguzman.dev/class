<?php 
class Recuperador{
    public static function Recuperar($email){
        $nuevaPass      = "";
        $nombreUsuario  = "";
        $conn           = new connbd();
        $strconn        = $conn->connect();
        
        #--- Limpia las varibales
        $email          = clear($email, $strconn);
        
        #--- Busca si existe un mail asociado al usuario que quiere recuperar
        $sql            = "SELECT count(email) as count, nombre FROM usuarios WHERE email = '$email' LIMIT 1";
        $res            = $strconn->query($sql) or die("Error get email: " . mysqli_error($strconn));

        if($res->num_rows > 0):
            $count  = $res->fetch_array();
            
            #--- si el contador de mail == 1 quiere decir que existe un registro coincidiendo
            if($count["count"] == 1):
                #--- genera una nueva password random
                $nuevaPass  = generaString(8);
                
                #--- se modifica la contraseña y estado a r = "Recuperación". Cuando inicie por primera vez
                #--- desde la recuperación deberá generar su contraseña
                $sql        = "UPDATE usuarios SET password = '$nuevaPass', estado='R' WHERE email = '$email'";
                $strconn->query($sql) or die ("Error update :" . mysqli_error());

                $msj        = "Hola ".$count["nombre"]."! <br><br>";
                $msj       .= "Tu contraseña ha sido actualizada correctamente, te la indicamos a continuación: <br><br>";
                $msj       .= "<b>Contraseña:</b> ".$nuevaPass." <br><br>";
                $msj       .= "Esta será tu nueva contraseña, la cual podrás actualizar al momento de iniciar sesión.";

                #--- se envía correo al usuario
                EnviarCorreo("Cambio de contraseña", $email, $msj);
                return true;
            else:
                return false;
            endif;
        endif;
    }

    public static function ActualizarContrasenas($usuario, $nuevaContrasena, $antiguaContrasena){
        $conn               = new connbd();
        $strconn            = $conn->connect();

        $nuevaContrasena    = clear($nuevaContrasena, $strconn);
        $usuario            = clear($usuario, $strconn);

        #--- valida que la contraseña antigua corresponda
        $query              = "SELECT password, nombre, email FROM usuarios WHERE codusuario = $usuario";
        $respuesta          = $strconn->query($query);
        $row                = $respuesta->fetch_array();
        
        if($row["password"] == $antiguaContrasena):

            $consulta           = "UPDATE usuarios SET password = '$nuevaContrasena', estado = 'A' WHERE codusuario = $usuario";
            $strconn->query($consulta) or die("Error update: " . mysqli_error($strconn));

            $msj        = "Hola ".$row['nombre']."! <br><br>";
            $msj       .= "Tu contraseña ha sido actualizada correctamente.<br/>";
            $msj       .= "Esta será tu nueva contraseña, la cual podrás actualizar al momento de iniciar sesión.";

            #--- se envía correo al usuario
            EnviarCorreo("Cambio de contraseña exitoso", $row["email"], $msj);
            return true;
        else:
            return false;
        endif;

    }

}


?>
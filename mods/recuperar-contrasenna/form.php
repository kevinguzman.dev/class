<?php 
session_start();
if(isset($_SESSION["estado"]) and $_SESSION["estado"] != "R" ):
    
    switch($_SESSION["estado"]){
        case "A":
            header("Location: ../dashboard/index");
            break;
        default:
            header("Location: ../login/index");
            break;
    }
endif;

require_once("../required/connbd.php");
require_once("../required/functions.php");
require_once("recuperador.php");

$Gd_json        = json_decode(file_get_contents("../required/config.json"));
$Gl_appName     = $Gd_json->{"appName"};
$Gl_appUrl      = $Gd_json->{"appUrl"};
$Gd_msj         = "";

if(isset($_POST["actual"]) and $_POST["actual"] != ""):
    if(Recuperador::ActualizarContrasenas($_SESSION["UserId"], $_POST["nueva"], $_POST["actual"]) ):
        header("Location: ../dashboard/index");
    else:
        $Gd_msj = "La contraseña antigua ingresado no corresponde";    
    endif;
    
elseif(isset($_POST["antigua"]) and $_POST["antigua"] == "" ):
    $Gd_msj = "Debes ingresar tu antigua contraseña";
endif;

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>kGym | Recuperar contraseña</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
  body{
    background-image: url("../img/sys/bg.jpg");
  }
</style>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="#"><b>k</b>GYM </a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php if($Gd_msj != ""):?>
            <div class="alert alert-danger fadeIn">
                <?= $Gd_msj ?>
            </div>
        <?php endif;?>
        <p class="login-box-msg">Ingresa los datos solicitados...</p>
        
        <form action="#" method="post">
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Contraseña actual" name="actual" id="actual" autocomplete="off" maxlength="12">
            </div>

            <div class="form-group has-feedback dvContrasena">
                <input type="password" class="form-control" placeholder="Contraseña nueva" name="nueva" id="nueva" autocomplete="off" maxlength="12">
            </div>

            <div class="form-group has-feedback dvContrasena">
                <input type="password" class="form-control" placeholder="Confirma la nueva contraseña" name="confirma" id="antigua" autocomplete="off" onchange="compararContrasenas();" maxlength="12">
                <span class="help-block" id="msjError" style="display:none;">Contraseñas no coinciden</span>
            </div>

            <div class="row">
                <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat" id="btn">Actualizar contraseña</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.login-box-body -->
  </div>

  <script src="<?=$Gl_appUrl ?>/mods/template/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
  <script type="text/javascript">
    compararContrasenas = function(){
        var con1 = $("#nueva").val();
        var con2 = $("#antigua").val();

        if(con1 != con2){
          $(".dvContrasena").addClass("has-error");
          $("#msjError").removeAttr("style");
          $("#btn").attr("disabled", "disabled");
        }else{
          $("#msjError").hide();
          $(".dvContrasena").removeClass("has-error");
          $("#btn").removeAttr("disabled");
        }
    }
  </script>
</body>
</html>

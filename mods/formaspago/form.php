<?php
require_once("../required/header.php");
require_once("formapago.php");
$Gd_accion          = "ADD";

$Gd_id          = "";
$Gd_descripcion = "";
$Gd_estado      = "A";

if(isset($_GET["id"]) && $_GET["id"] > 0):
  $Gd_id  = $_GET["id"];
  $Gd_fp = new FormaPago();

  $Gd_fp = $Gd_fp->Get($Gd_id);
  $Gd_descripcion   = $Gd_fp->descripcion;
  $Gd_estado        = $Gd_fp->estado;
endif;

if(isset($_POST["submit"])):
  $Gd_descripcion = $_POST["nombre"];
  $Gd_id          = $_POST["codigo"];

  if(isset($_POST["activo"])):
    $Gd_estado      = "A";
  else:
    $Gd_estado      = "I";
  endif;

  $Gd_fp = new FormaPago();
  $Gd_fp->descripcion = $Gd_descripcion;
  $Gd_fp->estado      = $Gd_estado;
  $Gd_fp->id          = $Gd_id;

  $Gd_fp->Save();
  $Gd_id          = $Gd_fp->id;
  $Gd_descripcion = $Gd_fp->descripcion;
  $Gd_estado      = $Gd_fp->estado;
endif;
?>

<section class="content-header">
  <h1>
    <?php if($Gd_accion == "ADD"): ?>
      Crear
    <?php else: ?>
      Modificar
    <?php endif; ?>
    Forma de pago
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/mods/formaspago/">Formas de pago</a></li>
    <li class="active">
      <?php if($Gd_accion == "ADD"): ?>
        Crear
      <?php else: ?>
        Modificar
      <?php endif; ?>
    </li>
  </ol>
</section>

<section class="content">

  <div class="row">
    <div class="col-xs-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#periodo" data-toggle="tab">Forma de pago</a></li>
        </ul>

        <div class="tab-content">
          <div class="tab-pane active" id="alumno">
            <form role="form" action="<?= $Gl_appUrl ?>/formaspago/form/<?=$Gd_id?>" method="POST">
              <input type="hidden" name="codigo" value="<?= $Gd_id ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="Nombre">Descripción</label>
                  <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese nombre de la forma de pago" value="<?= $Gd_descripcion ?>" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" class="minimal" name="activo" <?php if($Gd_estado == 'A'):?> checked="" <?php endif; ?>>
                     &nbsp Activo
                  </label>
                </div>
              </div>
              <div class="box-footer">
                <!--<button type="submit" name="submit" class="btn btn-primary" >Guardar</button>-->
                <button class="btn btn-primary" onclick="Guardar()">Guardar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">
  $('input[type="checkbox"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue'
  });

  Guardar = function(){
    swal({
      title: "Éxito",
      text: "forma de pago guardada con éxito, desea agregar otra?",
      buttons: ["Sí", "No"],
    });
  };
</script>
<?php require_once("../required/scripts.php"); ?>

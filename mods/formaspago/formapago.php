<?php
class FormaPago{
  public $id;
  public $descripcion;
  public $estado;

  public function GetAll(){
    $conn         = new connbd();
    $strconn      = $conn->connect();
    $sql          = "SELECT id, descripcion, estado FROM formaspago";
    $res          = $strconn->query($sql) or die ("Error get all: ". mysqli_error($strconn));
    $array        = array();

    if($res->num_rows > 0):
        while($row = $res->fetch_assoc()):
           $fp = new FormaPago();
           $fp->id          = $row["id"];
           $fp->descripcion = $row["descripcion"];
           $fp->estado      = $row["estado"];

           $array[] = $fp;
        endwhile;
    endif;

    $strconn->close();

    return $array;
  }

  public function Get($id){
    #--- datos de conexión
    $conn        = new connbd();
    $strconn     = $conn->connect();
    $sql         = "SELECT id, descripcion, estado FROM formaspago WHERE id = ".$id." LIMIT 1";
    $res         = $strconn->query($sql) or die ("Error get: ". mysqli_error($strconn));
    $fp          = new FormaPago();

    if($res->num_rows > 0):
      $row = $res->fetch_assoc();
      $fp->id           = $row["id"];
      $fp->descripcion  = $row["descripcion"];
      $fp->estado       = $row["estado"];
    endif;

    $strconn->close();
    return $fp;
  }

  public function Save(){
    #--- datos de conexión
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $sql = "";
    if($this->Exists()):
      $sql  = "UPDATE formaspago SET ";
      $sql .= " descripcion = '".$this->descripcion."', ";
      $sql .= " estado      = '".$this->estado."' ";
      $sql .= " WHERE id = ".$this->id;

      $strconn->query($sql) or die("error update: ". mysqli_error($strconn));
    else:
      $sql = "INSERT INTO formaspago (descripcion, estado) VALUES ('".$this->descripcion."', '".$this->estado."')";
      $strconn->query($sql) or die("error insert: ". mysqli_error($strconn));
      $this->id = $strconn->insert_id;
    endif;

    $strconn->close();
  }

  public function Exists(){
    if($this->id != ""):
        $conn         = new connbd();
        $strconn      = $conn->connect();
        $sql          = "SELECT count(id) as count FROM formaspago WHERE id = ".$this->id;
        $res          = $strconn->query($sql) or die ("Error exists: ". mysqli_error($strconn));

        if($res->num_rows > 0):
          $row = $res->fetch_assoc();
          if($row["count"] > 0):
            return true;
          else:
            return false;
          endif;
        endif;
    else:
      return false;
    endif;
  }


}

?>

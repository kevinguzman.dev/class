<?php
  require_once("../required/header.php");
  require_once("formapago.php");

  $Gd_fp    = new FormaPago();
  $Gd_fps   = json_encode($Gd_fp->GetAll());
?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Formas de pago
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Formas de pago</li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Listado de todas las formas de pago</h3>
        <div class="box-tools">
          <a href="<?= $Gl_appUrl ?>/formaspago/form" class="btn btn-default">Agregar nuevo</a>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="gastos" class="table table-striped responsive table-hover">
          <thead>
            <th>Código</th>
            <th>Descripción</th>
            <th>Acción</th>
          </thead>
        </table>
      </div>
    </div>
  </section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
  $('#gastos').DataTable({
    'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
    'paging'        : true,
    'lengthChange'  : true,
    'searching'     : true,
    'ordering'      : true,
    'info'          : true,
    'autoWidth'     : false,
    'responsive'    : true,
    'data'          : <?= $Gd_fps ?>,
    'columns'       : [
                        { data: "id" },
                        { data: "descripcion" },
                        {
                            sortable: false,
                            className: "table-view-pf-actions",
                            "render": function (data, type, row, meta) {
                              return "<a href='<?=$Gl_appUrl?>/formaspago/form/"+ row.id +"' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>";
                            }
                        },
                      ]
  })
})
</script>
<?php require_once("../required/scripts.php"); ?>

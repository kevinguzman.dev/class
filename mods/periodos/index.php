<?php
  require_once("../required/header.php");
  require_once("periodo.php");
  $per          = new Periodo();
  $Gd_periodos  = json_encode($per->GetAll());
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Periodos
    <small>Todos</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Periodos</li>
  </ol>
</section>

<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Listado de todos los períodos</h3>
      <div class="box-tools">
        <a href="<?= $Gl_appUrl ?>/periodos/form" class="btn btn-default">Crear nuevo</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="periodos" class="table table-striped responsive table-hover">
        <thead>
          <th>Id</th>
          <th>Nombre</th>
          <th>Estado</th>
          <th>Acción</th>
        </thead>
      </table>
    </div>
  </div>
</section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
  $('#periodos').DataTable({
    'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
    'paging'        : true,
    'lengthChange'  : true,
    'searching'     : true,
    'ordering'      : false,
    'info'          : true,
    'autoWidth'     : false,
    'responsive'    : true,
    'data'          : <?= $Gd_periodos ?>,
    'columns'       : [
                        { 
                          data: "codigo", 
                          sortable: false
                        },
                        { data: "nombre" },
                        {
                          sortable: false,
                          render: function (data, type, row, meta){
                            if(row.estado == "Cerrado"){
                              return "<span class='badge bg-green'>"+row.estado+"</span>";
                            }else{
                              //<span class='badge bg-yellow' onclick='cerrarMes("+ row.codigo +")'>"+row.estado+"</span>";
                              return "<a href='#' onclick='cerrarMes("+ row.codigo +")'><span class='badge bg-yellow'>"+row.estado+"</span></a> ";
                            }
                          }
                        },
                        {
                            sortable: false,
                            className: "table-view-pf-actions",
                            "render": function (data, type, row, meta) {
                              var url = "<a href='<?=$Gl_appUrl?>/periodos/form/"+ row.codigo +"' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>";
                              url += " <a href='<?=$Gl_appUrl?>/estadisticas/cierremes/"+ row.codigo +"' class='btn btn-default' title='Estadísticas'><i class='fa fa-bar-chart'></i></a>";
                              //url += "<a href='<?=$Gl_appUrl?>/periodos/pagos/"+ row.codigo +"' class='btn btn-default' title='Pagos asociados a este periodo'><i class='fa fa-dollar'></i></a>";
                              //url += "<a href='<?=$Gl_appUrl?>/periodos/gastos/"+ row.codigo +"' class='btn btn-default' title='Gastos asociados a este periodo'><i class='fa fa-money'></i></a>";
                              //url += "<a href='<?=$Gl_appUrl?>/periodos/impagos/"+ row.codigo +"' class='btn btn-default' title='Ver morosos'><i class='fa fa-frown-o'></i></a>";
                              return url;
                            }
                        },
                      ]
  })
});

cerrarMes = function(periodo){
  AlertConfirm("Confirmación", "Desea cerrar este período?", confirmarCierre, "warning", periodo);
}

confirmarCierre = function(resp, id){
  if(resp){
    //envia solicitud al server
    json = new Object();
    json["periodo"] = id;

    $.ajax({
      type: "POST",
      url: "<?= $Gl_appUrl?>/periodos/ajax",
      data: json,
      success: function(msj){
        if(msj == "OK"){
          AlertSuccess("Éxito", "Periodo cerrado con éxito", "<?= $Gl_appUrl?>/periodos/index");
        }else{
          AlertError("Ooops!", msj);
        }
      },
      error: function(msj){
        AlertError("Ooops!", msj.responseText);
      }
    });
  }
}
</script>

<?php require_once("../required/scripts.php"); ?>

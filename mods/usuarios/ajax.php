<?php 
session_start();
header("Content-type: text/html; charset=utf-8");
require_once("../required/functions.php");
require_once("../required/connbd.php");
require_once("usuario.php");


if(isset($_POST) && isset($_POST["accion"]) && $_POST["accion"] == "EST"):
    $estado     = $_POST["estado"];
    $nEstado    = "";
    
    if($estado == "A"): 
        $nEstado = "I";
    endif;

    if($estado == "I"):
        $nEstado = "A";
    endif;
    
    $obj                = new Usuario();
    $obj->codusuario    = $_POST["id"];
    $obj->estado        = $nEstado;

    $obj->CambiarEstado();
endif;

if(isset($_POST) && isset($_POST["accion"]) && $_POST["accion"] == "ADD"):
    $obj = new Usuario();
    
    $obj->codusuario    = $_POST["codigo"];
    $obj->nombre        = $_POST["nombre"];
    $obj->contrasena    = $_POST["password"];
    $obj->perfil        = $_POST["perfil"];
    $obj->login         = $_POST["username"];
    $obj->foto          = "no-photo.png";

    $obj->Save();
    
endif;

?>
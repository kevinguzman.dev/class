<?php
require_once("../required/header.php");
require_once("usuario.php");
#------------------
#---- Variables----
#------------------
$Gd_codusuario  = 0;
$Gd_nombre      = "";
$Gd_estado      = "";
$Gd_foto        = "";
$Gd_login       = "";
$Gd_contrasena  = "";
$Gd_accion      = "ADD";
$Gd_exito       = false;
$Gd_perfiles    = Usuario::GetPerfiles();
$Gd_perfil      = "";


if( isset($_GET["id"]) and $_GET["id"] > 0):
    $Gd_accion      = "GET";
    $Gd_codusuario  = $_GET["id"];
  
    $obj = new Usuario();
    $obj = $obj->Get($Gd_codusuario);
  
    $Gd_nombre      = $obj->nombre;
    $Gd_estado      = $obj->estado;
    $Gd_login       = $obj->login;
    $Gd_contrasena  = $obj->contrasena;
    $Gd_perfil      = $obj->perfil;
  endif;

?>


<section class="content-header">
    <h1>
        <?php if($Gd_accion == "ADD"): ?>
            Registrar
        <?php else: ?>
            Modificar
        <?php endif; ?>
        Usuario
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=$Gl_appUrl?>/perfiles/index">Perfiles</a></li>
        <li class="active">
            <?php if($Gd_accion == "ADD"): ?>
            Registrar
            <?php else: ?>
            Modificar
            <?php endif; ?>
        </li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#alumno" data-toggle="tab">Usuarios</a></li>
                    <li class="dropdown pull-right">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Opciones <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $Gl_appUrl ?>/usuarios/index">Ver todos</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $Gl_appUrl ?>/usuarios/form">Registrar nuevo</a></li>
                    </ul>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="alumno">
                        
                        <form role="form" action="<?= $Gl_appUrl ?>/usuarios/form/<?=$Gd_codusuario?>" method="POST">
                            <input type="hidden" name="codigo" value="<?= $Gd_codusuario ?>">
                            <div class="form-group">
                                <label for="Nombre">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese nombre del usuario" value="<?= $Gd_nombre ?>" autocomplete="off" required="">
                            </div>

                            <div class="form-group">
                                <label for="usuario">Usuario</label>
                                <input type="text" name="username" class="form-control" id="username" placeholder="Ingrese el nombre de usuario con el que se autenticará en el sistema" value="<?= $Gd_login ?>" autocomplete="off" required="">
                            </div>
                            
                            <div class="form-group dvContrasena">
                                <label for="contrasena">Contraseña</label>
                                <input type="password" name="contrasena" class="form-control" id="contrasena" placeholder="Ingrese contraseña del usuario" value="<?= $Gd_contrasena ?>" required="">
                            </div>

                            <div class="form-group dvContrasena">
                                <label for="contrasena2">Confirme contraseña</label>
                                <input type="password" name="contrasena2" class="form-control" id="contrasena2" placeholder="Reingrese contraseña del usuario" value="<?= $Gd_contrasena ?>" required="" onchange="compararContrasenas();">
                                <span class="help-block" id="msjError" style="display:none;">Contraseñas no coinciden</span>
                            </div>
                        
                            <div class="form-group">
                                <label for="estado">Perfil</label>
                                <select class="form-control" name="perfil" id="perfil" style="width: 100%;">
                                    <option value="0">---</option>
                                    <?php 
                                        for($i = 0; $i < count($Gd_perfiles); $i++):
                                            $Gd_select = "";
                                            if($Gd_perfiles[$i]->id == $Gd_perfil):
                                                $Gd_select = "selected=''";
                                            endif;?>
                                            <option value=<?=$Gd_perfiles[$i]->id?> <?= $Gd_select ?>><?=$Gd_perfiles[$i]->nombre?></option>
                                        <?php 
                                        endfor;?>
                                </select>

                            </div>
                            
                            <div class="box-footer">
                                <button type="button" class="btn btn-primary" id="btn">Guardar</button>
                            </div>
                        </form>
                    
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">
    compararContrasenas = function(){
        var con1 = $("#contrasena").val();
        var con2 = $("#contrasena2").val();

        if(con1 != con2){
          $(".dvContrasena").addClass("has-error");
          $("#msjError").removeAttr("style");
          $("#btn").attr("disabled", "disabled");
        }else{
          $("#msjError").hide();
          $(".dvContrasena").removeClass("has-error");
          $("#btn").removeAttr("disabled");
        }
    }

    $("#btn").click(function(){
        var json = new Object();
        
        json["codigo"]      = <?= $Gd_codusuario ?>;
        json["nombre"]      = $("#nombre").val();
        json["username"]    = $("#username").val();
        json["password"]    = $("#contrasena").val();
        json["perfil"]      = $("#perfil").val();
        json["accion"]      = "ADD";

        AlertConfirm("", "Desea guardar este usuario?", function(res){
            if(res){
                $.ajax({
                    type: "POST",
                    url: "<?= $Gl_appUrl ?>/usuarios/ajax",
                    data: json,
                    success: function(msj){
                        Success("Éxito", "Usuario guardado con éxito");
                        $("#perfil").val(json["perfil"]).change();
                    }
                });
            }
        }, "warning");
    });

</script>
<?php require_once("../required/scripts.php"); ?>
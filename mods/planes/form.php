<?php
require_once("../required/header.php");
require_once("plan.php");

$Gd_accion    = "ADD";
$Gd_id        = "";
$Gd_nombre    = "";
$Gd_valor     = "";
$Gd_estado    = "";
$Gd_duracion  = "";
$Gd_desde     = "";
$Gd_hasta     = "";

if(isset($_GET["id"]) and $_GET["id"] != 0):
  $Gd_id      = $_GET["id"];
  $obj        = new Plan();
  $obj        = $obj->Get($Gd_id);
  $Gd_nombre  = $obj->nombre;
  $Gd_valor   = $obj->valor;
  $Gd_estado  = $obj->estado;
endif;
?>
<section class="content-header">
  <h1>
    <?php if($Gd_accion == "ADD"): ?>
      Registrar
    <?php else: ?>
      Modificar
    <?php endif; ?>
    Plan
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/planes/index">Planes</a></li>
    <li class="active">
      <?php if($Gd_accion == "ADD"): ?>
        Registrar
      <?php else: ?>
        Modificar
      <?php endif; ?>
    </li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#gasto" data-toggle="tab">Plan</a></li>
          <li class="dropdown pull-right">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Opciones <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$Gl_appUrl?>/planes/form">Registrar nuevo</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$Gl_appUrl?>/planes/index">Ver todos</a></li>
          </ul>
        </ul>

        <div class="tab-content">
          <div class="tab-pane active" id="gasto">
            <form role="form" method="POST" id="form">
              <input type="hidden" id="id" value="<?=$Gd_id?>" />
              
              <div class="box-body">  
                <div class="form-group" id="dvNombre">
                  <label for="nombre">Nombre</label>
                  <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese nombre del plan" value="<?= $Gd_nombre  ?>" autocomplete="off">
                </div>

                <div class="form-group" id="dvValor">
                  <label for="valor">Valor</label>
                  <div class="input-group">
                    <span class="input-group-addon"><b>$</b></span>
                    <input type="number" class="form-control" id="valor" name="valor" placeholder="Ingrese valor del plan" value="<?= $Gd_valor  ?>" autocomplete="off">
                  </div>
                </div>

                <div class="form-group">
                  <label>Estado</label>
                  <select name="estado" id="estado" class="form-control">
                    <option value="A">Activo</option>
                    <option value="I">Inactivo</option>
                    <option value="E">Eliminado</option>
                  </select>
                </div>

                <!--
                <div class="form-group">
                  <label for="duracion">Duración</label>
                  <input type="checkbox" class="minimal" id="duracion" name="duracion" value="<?= $Gd_duracion  ?>" autocomplete="off">
                </div>

                <div class="form-group">
                  <label for="rango">Desde</label>
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>

                  <input type="text" class="form-control" id="rango">
                </div>-->

              </div>

              <div class="box-footer">
                <button type="button" class="btn btn-primary" name="submit" id="btn"> Guardar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
   </div>
 </div>
</section>
<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">
$("#btn").click(function(){
 Validar();
});

Validar = function(){
  var error = 0;

  if($("#valor").val() <= 0 || $("#valor").val() == ""){
    $("#dvValor").addClass("has-error");
    error = error +1;
  }else{
    $("#btn").removeAttr("disabled");
    $("#dvMonto").removeClass("has-error");
    error = error-1;
  }

  if($("#nombre").val() == ""){
    $("#dvNombre").addClass("has-error");
    error = error +1;
  }else{
    $("#btn").removeAttr("disabled");
    $("#dvNombre").removeClass("has-error");
    error = error -1;
  }

  if(error <= 0){
    AlertConfirm("Confirmación", "Desea guardar este plan?", Guardar, "warning");
  }
}


Guardar = function(resp){
  if(resp){
    var json        = new Object();
    json["id"]      = $("#id").val();
    json["valor"]   = $("#valor").val();
    json["nombre"]  = $("#nombre").val();
    json["estado"]  = $("#estado").val();

    $.ajax({
        url: '<?=$Gl_appUrl?>/planes/ajax',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function(data) {
          $("#id").val(data.obj.id);
          AlertSuccess("Éxito", "Plan guardado con éxito", "<?= $Gl_appUrl?>/planes/index");
        },
        error: function(data){
          console.log(data.responseText  );
        }
    });
  }
}
</script>
<?php require_once("../required/scripts.php"); ?>

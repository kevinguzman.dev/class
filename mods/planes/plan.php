<?php
  class Plan{
    public $id;
    public $nombre;
    public $valor;
    public $estado;

    public function GetAll(){
        $conn         = new connbd();
        $strconn      = $conn->connect();

        $sql          = "SELECT id, nombre, estado, valor FROM planes order by nombre";
        $res          = $strconn->query($sql) or die("Error getall: ".$strconn->mysqli_error());
        $Gd_array     = array();

        if($res->num_rows > 0):
          while($row = $res->fetch_assoc()):
              $obj = new Plan();
              $obj->id      = $row["id"];
              $obj->nombre  = $row["nombre"];
              $obj->estado  = $row["estado"];
              $obj->valor   = dinero($row["valor"]);
              $Gd_array[] = $obj;
          endwhile;
        endif;

        return $Gd_array;
    }

    public function Get($id){
      $conn         = new connbd();
      $strconn      = $conn->connect();
      $sql          = "select id, nombre, valor, estado from planes where id = ".$id." LIMIT 1";
      $res          = $strconn->query($sql) or die ("Error get: ".mysqli_error($strconn));
      $obj          = new Plan();

      if($res->num_rows > 0):
        $row          = $res->fetch_assoc();
        $obj->id      = $id;
        $obj->nombre  = $row["nombre"];
        $obj->estado  = $row["estado"];
        $obj->valor   = $row["valor"];
      endif;

      $strconn->close();
      return $obj;
    }

    public function Save(){
      if($this->id != null || $this->id != ""):
        if($this->Exists($this->id)):
            $this->Update();
        else:
            $this->Insert();
        endif;
      else:
        $this->Insert();
      endif;

      $Gd_msj = "OK";

      $array = array("msj" => $Gd_msj, "obj" => $this);
      return $array;
    }

    private function Insert(){
      $conn         = new connbd();
      $strconn      = $conn->connect();
      $sql      = "INSERT INTO planes (nombre, valor, estado) VALUES ('".$this->nombre."', '".$this->valor."', '".$this->estado."')";
      $res      = $strconn->query($sql) or die("Error insert: ".$strconn->mysqli_error());
      $this->id =  $strconn->insert_id;
      $strconn->close();
    }

    private function Update(){
      $conn         = new connbd();
      $strconn      = $conn->connect();
      $sql          = "UPDATE planes SET nombre = '".$this->nombre."', valor='".$this->valor."', estado = '".$this->estado."' WHERE id = ".$this->id;
      $res          = $strconn->query($sql) or die("Error update: ".$strconn->mysqli_error());
      $strconn->close();
    }

    public function Exists($id){
      $conn         = new connbd();
      $strconn      = $conn->connect();
      $sql          = "SELECT count(id) as count FROM planes where id = ".$id;
      $res          = $strconn->query($sql) or die("Error: " . mysqli_error($strconn));

      if($res->num_rows>0):
        $row = $res->fetch_assoc();
        if($row["count"] > 0):
            return true;
        else:
            return false;
        endif;
      endif;
    }

  }


 ?>

<?php
require_once("../required/header.php");
require_once("pago.php");
require_once("../periodos/periodo.php");

$Gd_accion        = "ADD";
$Gd_codigo        = "";
$Gd_alumno        = "";
$Gd_monto         = "";
$Gd_observaciones = "";
$Gd_responsable   = "";
$Gd_periodo       = "";
$Gd_formaPago     = "";
$Gd_planes     = Pago::TraerPlanes();
//pre($Gd_planes, true);
$Gd_planAlumno  = array();
$Gd_existePeriodoAbierto = Periodo::ExistsOpened();

#--- Trae los alumnos para llenar el combo box
$Gd_pago      = new Pago();
$Gd_alumnos   = $Gd_pago->GetAlumnos();
$Gd_periodos  = $Gd_pago->GetPeriodos();
$Gd_fPagos    = $Gd_pago->GetFormaPago();

if( isset($_POST["alumno"]) ):



  $Gd_accion = "GET";

endif;

if(isset($_GET["id"]) and $_GET["id"] != ""):
  $Gd_id      = $_GET["id"];
  $Gd_pago    = new Pago();
  $Gd_pago    = $Gd_pago->Get($Gd_id);

  $Gd_accion        = "GET";
  $Gd_codigo        = $Gd_pago->codigo;
  $Gd_alumno        = $Gd_pago->alumno;
  $Gd_monto         = $Gd_pago->monto;
  $Gd_observaciones = $Gd_pago->observaciones;
  $Gd_responsable   = $Gd_pago->responsable;
  $Gd_formaPago     = $Gd_pago->formaPago;
endif;
?>

<section class="content-header">
  <h1>
    <?php if($Gd_accion == "ADD"): ?>
      Registrar
    <?php else: ?>
      Modificar
    <?php endif; ?>
    Pago
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/pagos/index">Pagos</a></li>
    <li class="active">
      <?php if($Gd_accion == "ADD"): ?>
        Registrar
      <?php else: ?>
        Modificar
      <?php endif; ?>
    </li>
  </ol>
</section>

<section class="content">
  <?php if(!$Gd_existePeriodoAbierto): ?>
  <div class="callout callout-danger">
    <h4>
       Estimado Usuari@:
    </h4>
     <p>
      No existe periodo abierto, favor <a href="<?=$Gl_appUrl?>/periodos/form">haga click aquí</a> para abrir uno.
    </p>
   </div>
 <?php endif;?>
  <div class="row">
     <div class="col-xs-12">
       <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
           <li class="active"><a href="#gasto" data-toggle="tab">Pagos</a></li>
           <li class="dropdown pull-right">
             <a class="dropdown-toggle" data-toggle="dropdown" href="#">
               Opciones <span class="caret"></span>
             </a>
             <ul class="dropdown-menu">
               <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$Gl_appUrl?>/pagos/form">Registrar nuevo</a></li>
               <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$Gl_appUrl?>/pagos/index">Ver todos</a></li>
             </ul>
           </li>
         </ul>

           <div class="tab-content">
             <div class="tab-pane active" id="gasto">
               <form role="form" action="<?= $Gl_appUrl ?>/pagos/form/<?=$Gd_codigo?>" method="POST" id="form">
                 <input type="hidden" name="codigo" value="<?= $Gd_codigo ?>">
                 <div class="box-body">

                  <div class="form-group">
                    <label for="periodo">Período</label>
                    <select class="form-control" name="periodo" id="periodo">
                      <?php for($i = 0; $i< count($Gd_periodos); $i++): ?>
                        <option value="<?=$Gd_periodos[$i]->codigo ?>"><?=$Gd_periodos[$i]->nombre ?> </option>
                        <?php endfor;?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="alumno">Alumno</label>
                    <select class="form-control select2 select2-hidden-accessible" name="alumno" id="alumno" style="width: 100%;" <?php if($Gd_accion != "ADD"):?> disabled="" <?php endif; ?> tabindex="-1" aria-hidden="true">
                      <option value="0">---</option>
                    <?php  for($i = 0; $i < count($Gd_alumnos); $i++): ?>
                      <option value="<?=$Gd_alumnos[$i]["codigo"]?>" <?php if($Gd_alumnos[$i]["codigo"] == $Gd_alumno): ?> selected="" <?php endif; ?>>
                        <?=$Gd_alumnos[$i]["nombre"]?>
                      </option>
                    <?php endfor;?>
                    </select>
                    
                  </div>

                  <div class="form-group dvPlan">
                    <label for="plan">Plan/es</label>
                    <select name="plan[]" id="plan" class="form-control" multiple="multiple" data-placeholder="Seleccione uno o más planes" style="width: 100%;" tabindex="0">
                      <?php 
                        for($i = 0; $i < count($Gd_planes); $i++ ):
                          $Gd_select = "";
                          for($e = 0 ; $e < count($Gd_planAlumno); $e++):
                            if($Gd_planes[$i]->id == $Gd_planAlumno[$e]):
                              $Gd_select = "selected = ''";
                              break;
                            endif;
                          endfor;
                        ?>
                        <option value="<?= $Gd_planes[$i]->id ?>" <?= $Gd_select ?>><?= $Gd_planes[$i]->nombre ?></option>
                      <?php endfor; ?>
                    </select>
                  </div>

                  
                  <div class="form-group">
                    <label for="fpago">Forma de pago</label>
                    <select class="form-control select2" name="fpago" id="fpago" style="width: 100%;" <?php if($Gd_accion != "ADD"):?> disabled="" <?php endif; ?>>
                      <option value="0">---</option>
                    <?php  for($i = 0; $i < count($Gd_fPagos); $i++): ?>
                      <option value="<?=$Gd_fPagos[$i]->id?>" <?php if($Gd_fPagos[$i]->id == $Gd_formaPago): ?> selected="" <?php endif; ?>>
                        <?=$Gd_fPagos[$i]->descripcion?>
                      </option>
                    <?php endfor;?>
                    </select>
                  </div>
                

                <div class="form-group" id="dvMonto">
                  <label for="monto">Monto</label>
                  <div class="input-group">
                    <span class="input-group-addon"><b>$</b></span>
                    <input type="number" class="form-control" id="monto" name="monto" placeholder="Ingrese monto del pago" value="<?= $Gd_monto  ?>" <?php if($Gd_accion != "ADD"):?> disabled="" <?php endif; ?> onchange="Validar()" autocomplete="false">
                  </div>
                  <span class="help-block" id="msjError" style="display:none;">Debe ingresar un monto</span>
                </div>

                <div class="form-group">
                  <label for="observaciones">Observaciones</label>
                  <textarea class="form-control" name="observaciones" id="observaciones" rows="3" placeholder="Ingrese las observaciones del pago..." <?php if($Gd_accion != "ADD"):?> disabled="" <?php endif; ?>><?= $Gd_observaciones ?></textarea>
                </div>

                   <?php if($Gd_accion != "ADD"): ?>
                     <div class="form-group">
                       <label for="responsable">Responsable</label>
                       <input type="text" class="form-control" id="responsable" name="responsable" value="<?= $Gd_responsable ?>" disabled="">
                     </div>
                   <?php endif; ?>
                 </div>
                 <!-- /.box-body -->
               <?php
               $Gd_disabled = "";
                if(!$Gd_existePeriodoAbierto or $Gd_accion != "ADD"):
                  $Gd_disabled = 'disabled=""';
                endif;
                 ?>
                 <div class="box-footer">
                   <button type="button" class="btn btn-primary" name="submit" id="btn" <?= $Gd_disabled?>> Guardar</button>
                 </div>
               </form>
             </div>
           </div>
      </div>
   </div>
 </div>
</section>
<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">
$(function () {
  $(".select2").select2();
  $('#plan').select2();

  $("#plan").change(function(){
    AlertConfirm("Atención", "Realmente desea modificar los planes asociados al alumno?", function(res){
      var obj         = new Object();
      obj["planes"]   = $("#plan").val();
      obj["accion"]   = "PLN";
      $.ajax({
        url: '<?=$Gl_appUrl?>/pagos/ajax',
        type: 'post',
        dataType: 'json',
        data: obj,
        success: function(data) {
          $("#monto").val(data.suma);
        },
        error: function(data){
          console.log(data);
        }

      });
    }, "warning");

  });

  $("#alumno").change(function(){
    var idalumno      = $("#alumno").val();
    var json          = new Object();
    json["idAlumno"]  = idalumno;
    json["accion"]    = "ALN";

    $.ajax({
      url: '<?=$Gl_appUrl?>/pagos/ajax',
      type: 'post',
      dataType: 'json',
      data: json,
      success: function(data) {
        $("#monto").val(data.suma);
        var ids = "";
        for(i = 0; i < data.planes.length; i++){
          $("#plan option[value='" + data.planes[i].id + "']").attr("selected", true);
          $("#plan").select2();
        }        
      }, 
      error: function(data){
        console.log("error: " + data.responseText  );
      }
    });

  });

  $("#btn").click(function(){
    if(validar()){
      AlertConfirm("Atención", "Desea registrar este pago?", function(res){
        
        if(res){
          Load("Cargando...");
          var json = new Object();
          json["alumno"]          = $("#alumno").val();
          json["monto"]           = $("#monto").val();
          json["observaciones"]   = $("#observaciones").val();
          json["formaPago"]       = $("#fpago").val();
          json["planes"]          = $("#plan").val();
          json["accion"]          = "ADD";
          json["periodo"]         = $("#periodo").val();
          
          $.ajax({
            url: '<?=$Gl_appUrl?>/pagos/ajax',
            type: 'post',
            dataType: 'json',
            data: json,
            success: function(data) {
              AlertConfirm("Atención", "Pago realizado con éxito, desea seguir registrando pagos?", function(res){
                if(res)
                  window.location.replace("<?=$Gl_appUrl?>/pagos/form");
                else
                  window.location.replace("<?=$Gl_appUrl?>/pagos/index");

              }, "warning");
            },
            error: function(data){
              AlertError("Error", data.responseText);
            }
          });
        }

      }, "warning");
    }
  });

  validar = function(){
    if($("#monto").val() == ""){
      $("#msjError").show();
      $("#dvMonto").addClass("has-error");
      $("#msjError").removeAttr("style");
      $("#btn").attr("disabled", "disabled");
      return false;
    }else{
      $("#msjError").hide();
      $("#dvMonto").removeClass("has-error");
      $("#btn").removeAttr("disabled");
      return true;
    }
  }
  
  $("#monto").change(function(){
    validar();
  });

});





</script>
<?php require_once("../required/scripts.php"); ?>

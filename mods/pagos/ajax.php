<?php 
session_start();
header('Content-type: application/json');
require_once("../required/functions.php");
require_once("../required/connbd.php");
require_once("pago.php");


if(isset($_POST) && isset($_POST["idAlumno"]) && isset($_POST["accion"]) && $_POST["accion"] == "ALN"):
    #-- ejecuta esto cuando el idAlumno esté definido

    #--- llamar a la consulta de getPlanesAlumnos
    $idAlumno 	= $_POST["idAlumno"];
    $datos	  	= Pago::GetInfoAlumno($idAlumno); #--- esta función no está definida, por eso se cae y  el parámetro tiene que ir con $, corregir a $idAlumno.

    $suma 	 	= 0;
    #--- ciclo que sumará los valores de los planes
    for($i = 0; $i < count($datos); $i++){
    	#--- $suma toma el valor de los planes
    	$suma 	= $suma + $datos[$i]->valor;
    }

    #--- update kguzman - 28/02/2019 - retorna nuevo obj que contiene la suma y los planes asociados
    $obj 			= new stdClass();
    $obj->suma 		= $suma;
    $obj->planes 	= $datos;
    
    echo json_encode($obj, true);
endif;

if(isset($_POST) && isset($_POST["planes"]) && isset($_POST["accion"]) && $_POST["accion"] == "PLN"):
    $planes     = $_POST["planes"];
    $suma       = 0;

    if(count($planes) > 0){
        $suma = Pago::getSumaValorPlanes($planes);
    }

    $obj            = new stdClass();
    $obj->suma      = $suma;

    echo json_encode($obj, true);
endif;

if(isset($_POST) && isset($_POST["alumno"]) && isset($_POST["accion"]) && $_POST["accion"] == "ADD"):
    
    $Gd_pago                    = new Pago();
    $Gd_pago->alumno            = $_POST["alumno"];
    $Gd_pago->observaciones     = $_POST["observaciones"];
    $Gd_pago->monto             = limpiarNumero($_POST["monto"]);
    $Gd_pago->responsable       = $_SESSION["Login"];
    $Gd_pago->periodo           = $_POST["periodo"];
    $Gd_pago->formaPago         = $_POST["formaPago"];
    $Gd_pago->planes            = $_POST["planes"];
  
    $Gd_pago->Save();
    #--- obtiene el email del alumno
    $emailAlumno              = getEmailAlumno($Gd_pago->alumno);

    if($emailAlumno != ""){
        $nombreAlumno    = getNombreAlumno($Gd_pago->alumno);
        $planesAlumno    = getPlanesAlumnos($Gd_pago->alumno);

        $msj             = "Hola ".$nombreAlumno."! <br><br>";
        $msj            .= "Se ha registrado un pago en nuestra plataforma, el cual se detalla en lo siguiente: <br><br>";
        $msj            .= "<div style='text-align:left'><b>Plan</b></div>";
        $msj            .= "<div style='text-align:right'><b>Valor</b></div><hr>";

        for($i = 0; $i < count($planesAlumno); $i++){
          
          $msj .= "".$planesAlumno[$i]->nombre." - ".$planesAlumno[$i]->valor."<br>";
          
        } 

        $msj            .= "    </tbody><hr>";
        $msj            .= "<b>Total</b>: ".dinero($Gd_pago->monto)." <br><br>";
        $msj            .= "<b>Observaciones</b>: ".$Gd_pago->observaciones." <br></br>";
        $msj            .= "Saludos!";
    
        EnviarCorreo("Comprobante de pago", $emailAlumno, $msj);
    }

    $obj            = new stdClass();
    $obj->msj       = "OK";

    echo json_encode($obj, true);
endif;

if(isset($_POST) && isset($_POST["accion"]) && $_POST["accion"] == "DEL"):
    $pago                       = new Pago();
    $pago->codigo               = $_POST["pago"]; 
    $pago->motivoEliminacion    = $_POST["msj"];

    $pago->eliminarPago();

    $obj            = new stdClass();
    $obj->msj       = "OK";

    echo json_encode($obj, true);
endif;

?>



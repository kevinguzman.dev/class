<?php
  require_once("../required/header.php");
  require_once("espacio.php");

  $Gd_calendario    = new Espacio();
  $Gd_espacios      = json_encode($Gd_calendario->GetAll());
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Espacios
        <small>Todos</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Espacios</li>
    </ol>
</section>


<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Listado de todos los perfiles registrados</h3>
            <div class="box-tools">
                <a href="<?= $Gl_appUrl ?>/perfiles/form" class="btn btn-default">Agregar nuevo</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="espacios" class="table table-striped responsive table-hover">
                <thead>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </thead>
            </table>
        </div>
    </div>
</section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(document).ready(function(){
    $("#espacios").DataTable({
        'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
        'paging'        : true,
        'lengthChange'  : true,
        'searching'     : true,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'responsive'    : true,
        'data'          : <?= $Gd_espacios ?>,
        'columns'       : [
                            { data: "nombre" },
                            { 
                                sortable: false,
                                "render": function(data, type, row, meta){
                                    var est = "";
                                    if(row.estado == "A"){
                                        est = "Activo";
                                        lnk = "cambiarEstado(".concat("'", row.estado, "',", row.id, ")");

                                        return "<a href='#' onclick=" + lnk +"><span class='badge bg-green'>"+ est +"</span></a>";
                                    }else{
                                        est = "Inactivo";
                                        lnk = "cambiarEstado(".concat("'", row.estado, "',", row.id, ")");

                                        return "<a href='#' onclick=" + lnk +"><span class='badge bg-red'>"+ est +"</span></a>";
                                    }

                                }
                            },
                            {
                                sortable: false,
                                className: "table-view-pf-actions",
                                "render": function (data, type, row, meta) {
                                    return "<a href='<?=$Gl_appUrl?>/espacios/form/"+ row.id +"' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>";
                                }
                            },
                        ]
    });

    cambiarEstado = function(estado, id){
        AlertConfirm("", "Desea guardar este perfil?", function(res){
            if(res){
                var json = new Object();
                json["accion"]  = "EST";
                json["estado"]  = estado;
                json["id"]      = id;

                $.ajax({
                    type: "POST",
                    url: "<?= $Gl_appUrl ?>/perfiles/ajax",
                    data: json,
                    success: function(msj){
                        console.log(msj);
                        AlertSuccess("Éxito", "Perfil guardado con éxito", "<?= $Gl_appUrl?>/perfiles/index");
                    }
                });
            }
        }, 'warning');
    }
});
</script>
<?php require_once("../required/scripts.php"); ?>
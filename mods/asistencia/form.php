<?php
  require_once("../required/header.php");
  require_once("asistencia.php");

  $Gd_accion          = "ADD";
  $Gd_observaciones   = "";
  $Gd_fecha           = "";
  $Gd_obj             = new Asistencia();

  if(isset($_GET["id"]) && $_GET["id"] > 0):
    $Gd_accion        = "GET";
    $Gd_id            = $_GET["id"];
    $Gd_obj           = $Gd_obj->GetAsistencia($Gd_id);

    $Gd_profesor      = $Gd_obj->usuario;
    $Gd_fecha         = $Gd_obj->fecha;
    $Gd_observaciones = $Gd_obj->observaciones;
    $Gd_alumnos       = $Gd_obj->alumnos;
  else:
    $Gd_alumnos   = $Gd_obj->GetAlumnos();
    $Gd_profesor  = $_SESSION["User"];
    $Gd_fecha     = Hoy();
  endif;
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Registro de asistencia
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/asistencia/index">Asistencia</a></li>
    <li class="active">
      Registrar
    </li>
  </ol>
</section>

<section class="content">
  <div class="row">
      <div class="col-md-12">
           <!-- Horizontal Form -->
           <div class="box">
             <div class="box-header with-border">
               <h3 class="box-title">Clase</h3>
             </div>

             <form class="form-horizontal">
               <div class="box-body" style="padding: 10px;">

                 <div class="form-group">
                   <label for="profesor" class="col-sm-2 control-label">Profesor</label>
                   <div class="col-sm-10">
                     <input type="text" class="form-control" id="profesor" value="<?= $Gd_profesor ?>" disabled="">
                   </div>
                 </div>
                 <?php
                    $Gd_disabled ="";
                    if($Gd_accion == "GET"):
                      $Gd_disabled = 'disabled=""';
                    endif;
                   ?>
                 <div class="form-group">
                   <label for="fecha" class="col-sm-2 control-label">Fecha</label>
                   <div class="col-sm-10">
                     <input type="text" class="form-control" id="fecha" value="<?= $Gd_fecha ?>" <?=$Gd_disabled ?> >
                   </div>
                 </div>

                 <div class="form-group">
                   <label for="observaciones" class="col-sm-2 control-label">Observaciones</label>
                   <div class="col-sm-10">
                     <textarea name="observaciones" rows="2" class="form-control" id="observaciones" placeholder="Ingrese observaciones de la clase..."><?= $Gd_observaciones ?></textarea>
                   </div>
                 </div>
               </div>
             </form>
           </div>
         </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
          <?php if($Gd_accion == "ADD"): ?>
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Alumnos <small>(Clickear presentes)</small></h3>
            </div>
            <div class="box-body">
              <table class="table-responsive no-padding table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Rut</th>
                    <th>Nombre</th>
                  </tr>
                </thead>
                <tbody>
                  <?php for($i = 0; $i < count($Gd_alumnos); $i++): ?>
                    <tr>
                      <td><input type="checkbox" class="minimal" name="alumnos[]" value="<?=$Gd_alumnos[$i]->id ?>" /></td>
                      <td><?= $Gd_alumnos[$i]->rut ?></td>
                      <td><?= $Gd_alumnos[$i]->nombre ?></td>
                    </tr>
                  <?php endfor; ?>
                </tbody>

              </table>
            </div>
            <div class="box-footer" style="padding:10px;">
              <button type="button" class="btn btn-primary" onclick="Guardar();">Guardar</button>
            </div>
          </div>
        <?php else: ?>
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Alumnos <small>(presentes en esta clase)</small></h3>
            </div>
            <div class="box-body">
              <table class="table-responsive no-padding table table-hover">
                <thead>
                  <tr>
                    <th>Rut</th>
                    <th>Nombre</th>
                  </tr>
                </thead>
                <tbody>
                  <?php for($i = 0; $i < count($Gd_alumnos); $i++): ?>
                    <tr>
                      <td><?= $Gd_alumnos[$i]->rut ?></td>
                      <td><?= $Gd_alumnos[$i]->nombre ?></td>
                    </tr>
                  <?php endfor; ?>
                </tbody>

              </table>
            </div>
            <div class="box-footer" style="padding:10px;">
              <a href="<?=$Gl_appUrl?>/asistencia/index" class="btn btn-primary"> Regresar</a>
            </div>
          </div>
        <?php endif; ?>
        </div>
      </div>

</section>
<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">

$('#fecha').datepicker({
  autoclose: true,
  language: 'es',
  format: 'dd-mm-yyyy',
});
Guardar = function(){
  var alumnosChequeados   = $('input[name="alumnos[]"]:checked');
  var alumnos             = [];
  var json                = new Object();

  //obtiene los id de los alumnos marcados como presentes
  for(i = 0; i < alumnosChequeados.length; i++ ){
    var id = alumnosChequeados[i].value;
    alumnos.push(id);
  }

  json["fecha"]         = $("#fecha").val();
  json["observaciones"] = $("#observaciones").val();
  json["profesor"]      = $("#profesor").val();
  json["alumnos"]       = alumnos;
  json["codperiodo"]    = <?= $_SESSION["periodo"] ?>;
  json["accion"]        = "ADD";

  swal({
      title:                "Confirmación",
      text:                 "Está seguro de registrar esta asistencia?",
      showCancelButton:     true,
      confirmButtonText:    'Aceptar',
      cancelButtonText:     'Cancelar',
      confirmButtonClass:   "btn-danger",
      showLoaderOnConfirm:  false,
      allowOutsideClick:    false,
      allowEscapeKey:       false
  }).then(function (isConfirm) {
      if (isConfirm) {
          //envía peticion ajax
          $.ajax({
            type: "POST",
            url: "<?= $Gl_appUrl?>/asistencia/ajax",
            data: json,
            success: function(msj){
              if(msj == "OK"){
                AlertSuccess("Éxito", "Asistencia registrada con éxito", "<?= $Gl_appUrl?>/asistencia/index");
              }else{
                AlertError("Ooops!", msj);
              }
            },
            error: function(msj){
              AlertError("Ooops!", msj.responseText);
            }
          });
      }
  });

}
</script>
<?php require_once("../required/scripts.php"); ?>

<?php
require_once("../required/header.php");
require_once("asistencia.php");

$Gd_asistencia  = new Asistencia();
$Gd_asistencias = json_encode($Gd_asistencia->GetAll());
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Asistencias
    <small>registradas</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Asistencia</li>
  </ol>
</section>

<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Listado de todas las asistencias registradas</h3>
      <div class="box-tools">
        <a href="<?=$Gl_appUrl?>/asistencia/form" class="btn btn-default">Agregar nuevo</a>
      </div>
    </div>

    <!-- /.box-header -->
    <div class="box-body">
      <table id="asistencia" class="table table-striped responsive table-hover">
        <thead>
          <th>Creación</th>
          <th>Período</th>
          <th>Responsable</th>
          <th>Acción</th>
        </thead>
      </table>
    </div>
  </div>
</section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
  $('#asistencia').DataTable({
    'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
    'paging'        : true,
    'lengthChange'  : true,
    'searching'     : true,
    'ordering'      : true,
    'info'          : true,
    'autoWidth'     : false,
    'data'          : <?= $Gd_asistencias ?>,
    'responsive'    : true,
    'columns'       : [
                        { data: "fecha" },
                        { data: "nomperiodo" },
                        { data: "usuario" },
                        {
                            sortable: false,
                            className: "table-view-pf-actions",
                            "render": function (data, type, row, meta) {
                              return "<a href='<?=$Gl_appUrl?>/asistencia/form/"+ row.id +"' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>";
                            }
                        },
                      ]
  })
})
</script>
<?php require_once("../required/scripts.php"); ?>

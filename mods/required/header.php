<?php
  //verificar sesiones
  session_start();
  if( !isset($_SESSION["User"]) || !isset($_SESSION["Login"]) || $_SESSION == null):
    header("Location: ../../login/index");
  endif;
  //llama a funciones
  require_once("functions.php");
  require_once("connbd.php");
  //leer variables globales
  $Gl_appName   = "";
  $Gl_appUrl    = "";

  $Gd_json      = json_decode(file_get_contents("../required/config.json"));
  $Gl_appName   = $Gd_json->{"appName"};
  $Gl_appUrl    = $Gd_json->{"appUrl"};
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>kGym </title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/datatables.net-bs/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/SweetAlert/sweetalert2.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/fullcalendar/dist/fullcalendar.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/dist/css/AdminLTE.min.css?20200210">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/mods/template/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<?php require_once("sidebar.php"); ?>

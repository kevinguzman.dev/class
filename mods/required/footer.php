</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.1.0
  </div>
  <strong>Copyright &copy; 2017-2018 <a href="https://www.ksolutions.cl">kSolutions</a>.</strong> Todos los derechos reservados
</footer>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/SweetAlert/sweetalert2.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/SweetAlert/core.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/SweetAlert/Alerts.js"></script>

<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/plugins/input-mask/jquery.inputmask.numeric.extensions.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/moment/min/moment.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/dist/js/adminlte.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/plugins/iCheck/icheck.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/datatables.net-bs/js/dataTables.responsive.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/chart.js/Chart.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?=$Gl_appUrl ?>/mods/template/bower_components/fullcalendar/dist/locale-all.js"></script>

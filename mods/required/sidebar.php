<?php
  //trae la foto del usuario:
  $Gl_userFoto  = traerFotoUsuario($Gl_appUrl, $_SESSION["Login"]);
  $Gl_perfil    = $_SESSION["Perfil"];
?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?= $Gl_appUrl ?>/dashboard/index" class="logo">
      <span class="logo-mini"><b>k</b>GYM</span>
      <span class="logo-lg"><b>k</b>GYM</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=$Gl_userFoto ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= $_SESSION["User"] ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=$Gl_userFoto ?>" class="img-circle" alt="User Image">
                <p>
                  <?= $_SESSION["User"]?>
                  <small>Miembro desde <?= $_SESSION["userDate"]?></small>
                </p>
              </li>

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="<?=$Gl_appUrl ?>/login/index/out" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=$Gl_userFoto ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= $_SESSION["User"]?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online / Perfil: <?=$_SESSION["NomPerfil"]?></a><br>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menú / Opciones</li>
        <li>
          <a href="<?= $Gl_appUrl ?>/dashboard/index">
            <i class="fa fa-dashboard"></i> <span>Escritorio</span>
          </a>
        </li>

        <?php if(ValidarAccesoModulo($Gl_perfil, 1)): ?>
          <li>
            <a href="<?= $Gl_appUrl ?>/periodos/index">
              <i class="fa fa-calendar"></i> <span>Períodos</span>
            </a>
          </li>
        <?php endif; ?>
        
        <?php if(ValidarAccesoModulo($Gl_perfil, 5)): ?>
        <!--
        <li class="treeview">
          <a href="#">
            <i class="fa fa-calendar-minus-o"></i>
            <span>Administrar espacios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= $Gl_appUrl ?>/adm_espacios/index"><i class="fa fa-circle-o"></i> Espacios</a></li>
          </ul>
        </li>-->
        <?php endif;?>

        <?php if(ValidarAccesoModulo($Gl_perfil, 2)): ?>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i>
              <span>Alumnos</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?= $Gl_appUrl ?>/alumnos/index"><i class="fa fa-circle-o"></i> Ver todos</a></li>
              <li><a href="<?= $Gl_appUrl ?>/alumnos/form"><i class="fa fa-circle-o"></i> Registrar</a></li>
            </ul>
          </li>
        <?php endif;?>

        <?php if(ValidarAccesoModulo($Gl_perfil, 4)): ?>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-money"></i>
              <span>Pagos</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?= $Gl_appUrl ?>/pagos/form"><i class="fa fa-circle-o"></i> Registrar pago</a></li>
              <li><a href="<?= $Gl_appUrl ?>/pagos/index"><i class="fa fa-circle-o"></i> Ver pagos</a></li>
              <li><a href="<?= $Gl_appUrl ?>/gastos/form"><i class="fa fa-circle-o"></i> Registrar gasto</a></li>
              <li><a href="<?= $Gl_appUrl ?>/gastos/index"><i class="fa fa-circle-o"></i> Ver gastos</a></li>
            </ul>
          </li>
        <?php endif; ?>
        
        <?php if(ValidarAccesoModulo($Gl_perfil, 3)): ?>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-hand-o-up"></i>
              <span>Asistencia</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?= $Gl_appUrl ?>/asistencia/form"><i class="fa fa-circle-o"></i> Registrar</a></li>
              <li><a href="<?= $Gl_appUrl ?>/asistencia/index"><i class="fa fa-circle-o"></i> Ver todas</a></li>
            </ul>
          </li>
        <?php endif;?>

        <?php if(ValidarAccesoModulo($Gl_perfil, 5)): ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart"></i>
            <span>Estadísticas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= $Gl_appUrl ?>/periodos/index"><i class="fa fa-circle-o"></i> Cierre de mes</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Asistencia</a></li>
          </ul>
        </li>
        <?php endif;?>

        <?php if(ValidarAccesoModulo($Gl_perfil, 6)): ?>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-cogs"></i> <span>Configuración</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?= $Gl_appUrl ?>/planes/index"><i class="fa fa-circle-o"></i> Planes</a></li>
              <li><a href="<?= $Gl_appUrl ?>/formaspago/index"><i class="fa fa-circle-o"></i> Formas de pago</a></li>
              <li><a href="<?= $Gl_appUrl ?>/usuarios/index"><i class="fa fa-circle-o"></i> Usuarios</a></li>
              <li><a href="<?= $Gl_appUrl ?>/perfiles/index"><i class="fa fa-circle-o"></i> Perfiles</a></li>
              <li><a href="<?= $Gl_appUrl ?>/espacios/index"><i class="fa fa-circle-o"></i> Espacios</a></li>
            </ul>
          </li>
        <?php endif; ?>
        <?php if(ValidarAccesoModulo($Gl_perfil, 7)): ?>
          <li>
            <a href="<?= $Gl_appUrl ?>/mis-pagos/index">
              <i class="fa fa-money"></i> <span>Mis pagos</span>
            </a>
          </li>
        <?php endif; ?>

        <!---
        <li class="header">Sitio web</li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-hand-o-up"></i>
            <span>Artículos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Ver todos</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-hand-o-up"></i>
            <span>Secciones</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Ver todos</a></li>
          </ul>
        </li>

        <li class="header">Tienda virtual</li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-hand-o-up"></i>
            <span>Productos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Ver todos</a></li>
          </ul>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

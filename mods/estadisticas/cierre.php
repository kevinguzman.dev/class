<?php
class Cierre{
    public $totalIngresos;
    public $totalEgresos;
    public $estado;
    public $nombrePeriodo;

    function __construct(){}

    function Get($id){
      $conn         = new connbd();
      $strconn      = $conn->connect();
      $Gd_cierre    = new Cierre();

      #--- obtiene "cabecera" del periodo
      $sql = "select nombre, estado from periodos where codperiodo = ".$id." LIMIT 1";
      $res = $strconn->query($sql) or die ("Error: ".mysqli_error($strconn));

      if($res->num_rows > 0):
        $row = $res->fetch_assoc();
        $Gd_cierre->nombrePeriodo   = $row["nombre"];
        $Gd_cierre->estado          = estadoPeriodo($row["estado"]);
      endif;

      #--- obtiene el total de los ingresos (pagos)
      $sql = "select ifnull(sum(monto),0) as total from pagos where codperiodo = ".$id;
      $res = $strconn->query($sql) or die ("Error: ".mysqli_error($strconn));

      if($res->num_rows > 0):
        $row = $res->fetch_assoc();
        $Gd_cierre->totalIngresos = $row["total"];
      else:
        $Gd_cierre->totalIngresos = 0;
      endif;

      #--- obtiene el total de los egresos (gastos)
      $sql = "select ifnull(sum(monto),0) as total from gastos where codperiodo = ".$id;
      $res = $strconn->query($sql) or die ("Error: ".mysqli_error($strconn));
      
      if($res->num_rows > 0):
        $row = $res->fetch_assoc();
        $Gd_cierre->totalEgresos = $row["total"];
      else:
        $Gd_cierre->totalEgresos = 0;
      endif;

      return $Gd_cierre;
    }
}
?>

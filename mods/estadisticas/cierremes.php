<?php
#--- valida que siempre venga un valor en el get para que no se caiga por id
if(!isset($_GET["id"]) or ($_GET["id"] == 0) or ($_GET["id"] == "") ):
  header("Location: ../../periodos/index");
endif;

#--- obtiene el layout y la clase cierre
require_once("../required/header.php");
require_once("cierre.php");
require_once("../periodos/periodo.php");

#--- inicialización de variables
$Gd_codPeriodo      = $_GET["id"];
$Gd_periodo         = "";
$Gd_estado          = "";
$Gd_totalIngresos   = 0;
$Gd_totalEgresos    = 0;

$Gd_totalIngresosA  = 0;
$Gd_totalEgresosA   = 0;
$Gd_periodoAnterior = "";

#--- obtiene los datos del cierre
$Gd_cierre          = new Cierre();
$per                = new Periodo();

#--- obtiene el último mes cerrado
$Gd_ultimoPeriodo   = $per->getUltimoPeriodoCerrado();

$Gd_cierre          = $Gd_cierre->Get($Gd_codPeriodo);
$Gd_cierreAnterior  = $Gd_cierre->Get($Gd_ultimoPeriodo);

$Gd_periodo         = $Gd_cierre->nombrePeriodo;
$Gd_estado          = $Gd_cierre->estado;
$Gd_totalEgresos    = $Gd_cierre->totalEgresos;
$Gd_totalIngresos   = $Gd_cierre->totalIngresos;

$Gd_totalIngresosA  = $Gd_cierreAnterior->totalIngresos;
$Gd_totalEgresosA   = $Gd_cierreAnterior->totalEgresos;
$Gd_periodoAnterior = $Gd_cierreAnterior->nombrePeriodo;

$Gd_gastos  	      = json_encode($per->GetGastos($Gd_codPeriodo));
$Gd_cPagos		      = $per->Get($Gd_codPeriodo);
$Gd_pagos  		      = json_encode($per->GetPagos($Gd_codPeriodo));
$Gd_impagos         = json_encode($per->GetImpagos($Gd_codPeriodo));


?>
<section class="content-header">
  <h1>
    Reportes del período: <small><b><?= $Gd_periodo ?></b></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/periodos/index">Períodos</a></li>
    <li class="active">
      Estadística fin de mes
    </li>
  </ol>
</section>


<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">

        <div class="box-header with-border">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="box-title"><b>Periodo anterior:</b> <?= $Gd_periodoAnterior ?>  / <b>Estado:</b> Cerrado</h3>
              </div>
              <div class="col-sm-6">
                <h3 class="box-title">
                  <b>Reporte del período: </b><?= $Gd_periodo ?> / <b>Estado:</b> <?= $Gd_estado ?>
                </h3>
              </div>
            </div>
        </div>

        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="chart">
                <canvas id="periodoAnterior" style="height: 180px;"></canvas>
              </div>
            </div>

            <div class="col-md-6">
              <div class="chart">
                <canvas id="periodoActual" style="height: 180px;"></canvas>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div>

      <div class="box-footer">
        <div class="row">
          <div class="col-sm-6 col-xs-6">
            <div class="description-block border-right">
              <h5 class="description-header"><?= dinero($Gd_totalIngresosA) ?></h5>
              <span class="description-text">TOTAL PAGOS (Periodo anterior)</span>
            </div>
          </div>
          <div class="col-sm-6 col-xs-6">
            <div class="description-block border-right">
              <h5 class="description-header"><?= dinero($Gd_totalIngresos) ?></h5>
              <span class="description-text">TOTAL PAGOS</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-xs-6">
          <div class="description-block border-right">
            <h5 class="description-header"><?= dinero($Gd_totalEgresosA) ?></h5>
            <span class="description-text">TOTAL GASTOS (Periodo anterior)</span>
          </div>
        </div>
        <div class="col-sm-6 col-xs-6">
          <div class="description-block border-right">
            <h5 class="description-header"><?= dinero($Gd_totalEgresos) ?></h5>
            <span class="description-text">TOTAL GASTOS</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- GASTOS -->
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Gastos registrados </b></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="gastos" class="table table-striped responsive table-hover">
        <thead>
          <th>Creación</th>
          <th>Monto</th>
          <th>Observación</th>
          <th>Responsable</th>
        </thead>
      </table>
    </div>
  </div>
</section>

<!-- PAGOS -->
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Pagos registrados </b></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="pagos" class="table table-striped responsive table-hover">
        <thead>
          <th>Creación</th>
          <th>Monto</th>
          <th>Observación</th>
          <th>Alumno</th>
					<th>Responsable</th>
        </thead>
      </table>
    </div>
  </div>
</section>

<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Alumnos morosos </b></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="impagos" class="table table-striped responsive table-hover">
        <thead>
          <th>Rut</th>
          <th>Alumno</th>
          <th>Último Periodo Pagado</th>
        </thead>
      </table>
    </div>
  </div>
</section>


<?php require_once("../required/footer.php");?>
<script type="text/javascript">
var ctx   = document.getElementById("periodoActual").getContext("2d");
var ctxA  = document.getElementById("periodoAnterior").getContext("2d");

var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Gastos", "Pagos"],
        datasets: [{
            data: [<?=$Gd_totalEgresos ?>, <?= $Gd_totalIngresos ?>],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
      responsive: true,
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: '<?= $Gd_periodo ?>'
      },
      legend: {
        display: false
      },
      scales: {
          yAxes: [{
              ticks: {
                  beginAtZero:true
              }
          }]
      }
}
});


var myChart2 = new Chart(ctxA, {
    type: 'bar',
    data: {
        labels: ["Gastos", "Pagos"],
        datasets: [{
            data: [<?=$Gd_totalEgresosA ?>, <?= $Gd_totalIngresosA ?>],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
      responsive: true,
      legend: { position: 'top' },
      title: {
        display: true,
        text: '<?= $Gd_periodoAnterior ?>'
      },
      legend: {
        display: false
      },
      scales: {
          yAxes: [{
              ticks: {
                  beginAtZero:true
              }
          }]
      }
    }
});

$('#gastos').DataTable({
  'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
  'paging'        : true,
  'lengthChange'  : true,
  'searching'     : true,
  'ordering'      : false,
  'info'          : true,
  'autoWidth'     : false,
  'data'          : <?= $Gd_gastos ?>,
  'columns'       : [
                      { data: "fecha" },
                      { data: "monto" },
                      { data: "descripcion" },
                      { data: "responsable" }
                    ]
});

$('#pagos').DataTable({
  'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
  'paging'        : true,
  'lengthChange'  : true,
  'searching'     : true,
  'ordering'      : false,
  'info'          : true,
  'autoWidth'     : false,
  'data'          : <?= $Gd_pagos ?>,
  'columns'       : [
                      { data: "creacion" },
                      { data: "monto" },
                      { data: "observacion" },
                      { data: "alumno" },
                      { data: "responsable" },
                    ]
});

$('#impagos').DataTable({
  'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
  'paging'        : true,
  'lengthChange'  : true,
  'searching'     : true,
  'ordering'      : false,
  'info'          : true,
  'autoWidth'     : false,
  'data'          : <?= $Gd_impagos ?>,
  'columns'       : [
                      { data: "rut" },
                      { data: "nombre" },
                      { data: "ultPeriodo" }
                    ]
})
</script>
<?php require_once("../required/scripts.php"); ?>

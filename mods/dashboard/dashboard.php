<?php
class Dashboard{

    public function GetData(){
        $conn               = new connbd();
        $strconn            = $conn->connect();
        $Gd_nomPeriodo      = "";
        $Gd_idPeriodo       = 0;
        $Gd_totalPagos      = 0;
        $Gd_totalAlumnos    = 0;
        $Gd_totalGastos     = 0;
        $Gd_promAsistencia  = 0;

        #--- obtiene el periodo actual activo
        $sql = "select codperiodo, nombre from periodos where codperiodo = (select max(codperiodo) from periodos)";
        $res = $strconn->query($sql) or die ("Error al traer periodo: ". mysqli_error($strconn) );

        if($res->num_rows > 0):
            $row = $res->fetch_assoc();
            $Gd_idPeriodo = $row["codperiodo"];
            $Gd_nomPeriodo = $row["nombre"];
        endif;

        #--- obtiene el total de pagos recaudados en el mes
        $sql = "select COUNT(codpago) as count from pagos where codperiodo = ".$Gd_idPeriodo . " AND estado = 'A'";
        $res = $strconn->query($sql) or die ("Error al traer pagos: ". mysqli_error($strconn) );

        if($res->num_rows > 0):
            $row = $res->fetch_assoc();
            $Gd_totalPagos = $row["count"];
        endif;

        #--- obtiene el total de alumnos registrados en el mes
        $sql = "select COUNT(codalumno) as count from alumno where codperiodo = ".$Gd_idPeriodo;
        $res = $strconn->query($sql) or die ("Error al traer alumnos: ". mysqli_error($strconn) );

        if($res->num_rows > 0):
            $row = $res->fetch_assoc();
            $Gd_totalAlumnos = $row["count"];
        endif;

        #--- obtiene el total de gastos registrados en el mes
        $sql = "select COUNT(codgasto) as count from gastos where codperiodo = ".$Gd_idPeriodo;
        $res = $strconn->query($sql) or die ("Error al traer gastos: ". mysqli_error($strconn) );

        if($res->num_rows > 0):
            $row = $res->fetch_assoc();
            $Gd_totalGastos = $row["count"];
        endif;

        #--- obtiene el promedio de asistencia
        #-- pendiente

        $strconn->close();

        $obj = new stdClass();

        $obj->nomPeriodo      = $Gd_nomPeriodo;
        $obj->idPeriodo       = $Gd_idPeriodo;
        $obj->totalPagos      = $Gd_totalPagos;
        $obj->totalAlumnos    = $Gd_totalAlumnos;
        $obj->totalGastos     = $Gd_totalGastos;
        $obj->promAsistencia  = $Gd_promAsistencia;

        return $obj;
    }

    public function GetGraphData(){
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        $conn               = new connbd();
        $strconn            = $conn->connect();
        
        //obtiene total de pagos por mes
        $sql  = "select";
        $sql .= " t2.nombre, ";
        $sql .= " sum(ifnull(t1.monto,0)) as pago ";
        $sql .= " from pagos t1 ";
        $sql .= " inner join periodos t2 on t1.codperiodo = t2.codperiodo ";
        $sql .= " WHERE t1.estado = 'A' ";
        $sql .= " group by t2.codperiodo, t2.nombre ";
        $sql .= " ORDER BY t2.codPeriodo DESC ";
        $sql .= " LIMIT 6";

        $res = $strconn->query($sql) or die ("Error al traer grafico: ". mysqli_error($strconn) );
        
        $Gd_array       = array();
        $Gd_pagos       = array();
        $Gd_periodos    = array();
        $Gd_gastos      = array();

        if($res->num_rows > 0):
            while($row = $res->fetch_assoc()):
                $Gd_pagos[]     = $row["pago"];
                $Gd_periodos[]  = $row["nombre"];
            endwhile;
        endif;

        $sql2  = "SELECT t2.nombre, sum(ifnull(t1.monto,0)) as gasto  ";
        $sql2 .= "FROM gastos t1 ";
        $sql2 .= "INNER JOIN periodos t2 on t1.codperiodo = t2.codperiodo ";
        $sql2 .= "GROUP BY t2.codperiodo, t2.nombre ";
        $sql2 .= "ORDER BY t2.codperiodo DESC LIMIT 6";

        $res2  = $strconn->query($sql2) or die ("Error gastos: " . mysqli_error($strconn));

        if($res->num_rows > 0):
            while($row = $res2->fetch_array()):
                $Gd_gastos[] = $row["gasto"];
            endwhile;
        endif;

        krsort($Gd_periodos);
        krsort($Gd_pagos);
        krsort($Gd_gastos);

        $Gd_periodos    = array_values($Gd_periodos);
        $Gd_pagos       = array_values($Gd_pagos);
        $Gd_gastos      = array_values($Gd_gastos);

        $Gd_periodosRet = array();
        $Gd_pagosRet    = array();
        $Gd_gastosRet   = array();

        for($i = 0; $i < count($Gd_periodos); $i++):
            $Gd_periodosRet[]  = $Gd_periodos[$i];
            $Gd_pagosRet[]     = $Gd_pagos[$i];
            $Gd_gastosRet[]    = $Gd_gastos[$i];
        endfor;

        #--- para testear
        /*
        echo "periodos: ";
        pre($Gd_periodosRet, false);
        echo "pagos: ";
        pre($Gd_pagosRet, false);
        echo "gastos: ";
        pre($Gd_gastosRet, false);
        */

        $Gd_obj             = new stdClass();
        $Gd_obj->pagos      = $Gd_pagosRet;
        $Gd_obj->periodos   = $Gd_periodosRet;
        $Gd_obj->gastos     = $Gd_gastosRet;

        $Gd_array[]         = $Gd_obj;
        $strconn->close();

        return $Gd_array;
    }

    public static function getPagos($idUsuario){
        $array              = array();
        $conn               = new connbd();
        $strconn            = $conn->connect();
        $consulta           =  "SELECT codpago, t1.creacion, monto, t2.descripcion, t3.nombre
                                FROM pagos t1 
                                INNER JOIN formaspago t2 ON t1.idformapago = t2.id
                                INNER JOIN usuarios t3 ON t1.usuario = t3.login
                                INNER JOIN usuarios t4 ON t1.codalumno = t4.id_alumno
                                WHERE t4.codusuario = '$idUsuario'";
        
        $respuesta          = $strconn->query($consulta) or die ("Error getPagos: ". mysqli_error($strconn));

        while($row = $respuesta->fetch_assoc()){
            $obj            = new stdClass();
            $obj->id        = $row["codpago"];
            $obj->fecha     = $row["creacion"];
            $obj->monto     = dinero($row["monto"]);
            $obj->fPago     = $row["descripcion"];

            $array[]        = $obj;
        }

        return $array;
    }
}


?>

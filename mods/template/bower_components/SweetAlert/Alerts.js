﻿function Load(title) {

    swal({
        title: title,
        allowOutsideClick: false,
        allowEscapeKey: false
    });

    swal.showLoading();
}

function AlertSuccess(title, text, url) {

    swal({
        title: title,
        text: text,
        type: 'success',
        showCancelButton: false,
        confirmButtonText: 'Aceptar',
        showLoaderOnConfirm: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(function (isConfirm) {
        if (isConfirm) {
            window.location.href = url;
        }
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            return false;
        }
    });
}

function AlertError(title, text) {

    swal({
        title: title,
        html: text,
        type: 'error',
        showCancelButton: false,
        confirmButtonText: 'Aceptar',
        showLoaderOnConfirm: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function AlertConfirm(title, text, Callback, tipoAlerta ) {

    swal({
        title: title,
        text: text,
        type: tipoAlerta,
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        confirmButtonClass: "btn-danger",
        showLoaderOnConfirm: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(function (isConfirm) {
        if (isConfirm) {
            Callback(true);
        }
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            Callback(false);
        }
    });
}

function AlertConfirm(title, text, Callback, tipoAlerta, id ) {

    swal({
        title: title,
        text: text,
        type: tipoAlerta,
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        confirmButtonClass: "btn-danger",
        showLoaderOnConfirm: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(function (isConfirm) {
        if (isConfirm) {
            Callback(true, id);
        }
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            Callback(false, id);
        }
    });
}

function Success(title, text) {
    swal({
        title: title,
        text: text,
        type: 'success',
        showCancelButton: false,
        confirmButtonText: 'Aceptar',
        showLoaderOnConfirm: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function Warning(title, text) {
    swal({
        title: title,
        text: text,
        type: 'warning',
        showCancelButton: false,
        confirmButtonText: 'Aceptar',
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

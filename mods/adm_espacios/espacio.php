<?php 
class Espacio{
    public function GetEspacios(){
        #--- datos de conexión
        $conn           = new connbd();
        $strconn        = $conn->connect();
        $sql            = "SELECT id, nombre, estado FROM espacio ORDER BY id";
        $res            = $strconn->query($sql) or die("Error getEspacios: ".mysqli_error($strconn));
        $array          = array();
        
        while($row = $res->fetch_array()):
            $obj            = new stdClass();
            $obj->id        = $row["id"];
            $obj->nombre    = $row["nombre"];
            $obj->estado    = $row["estado"];

            $array[]        = $obj;
        endwhile;

        $strconn->close();
        return $array;
    }
}



?>
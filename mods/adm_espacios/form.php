<?php
    if(!isset($_GET) && !is_numeric($_GET["id"]) ):
        echo "asda";exit;
        header("Location: index.php");
    endif;

    require_once("../required/header.php");
    require_once("espacio.php"); 
?>

<section class="content-header">
  <h1>
    Calendario del espacio: [espacio]
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/asistencia/index">Asistencia</a></li>
    <li class="active">
      Registrar
    </li>
  </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <!-- THE CALENDAR -->
                <div id="calendar"></div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
		<div class="modal-content">
            <form class="form-horizontal" method="POST" action="addEvent.php">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Evento</h4>
                </div>
                
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Titulo</label>
                        <div class="col-sm-10">
                            <input type="text" name="title" class="form-control" id="title" placeholder="Titulo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="color" class="col-sm-2 control-label">Color</label>
                        <div class="col-sm-10">
                            <select name="color" class="form-control" id="color">
                                <option value="">Seleccionar</option>
                                <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
                                <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
                                <option style="color:#008000;" value="#008000">&#9724; Verde</option>						  
                                <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
                                <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
                                <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
                                <option style="color:#000;" value="#000">&#9724; Negro</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="start" class="col-sm-2 control-label">Fecha Inicial</label>
                        <div class="col-sm-10">
                            <input type="text" name="start" class="form-control" id="start" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="end" class="col-sm-2 control-label">Fecha Final</label>
                        <div class="col-sm-10">
                            <input type="text" name="end" class="form-control" id="end" readonly>
                        </div>
                    </div>
			    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
			</form>
        </div>
    </div>
</div>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear();

    $('#calendar').fullCalendar({
        locale: 'es',
        header    : {
            left  : 'prev,next today',
            center: 'title',
            right : 'month,agendaWeek,agendaDay'
        },
        buttonText: {
            today: 'Hoy',
            month: 'Mensual',
            week : 'Semanal',
            day  : 'Día'
        },
        editable: true,
        eventLimit: true,
        selectable: true,
        select: function(start, end){
            
        }
    });

});

</script>
<?php require_once("../required/scripts.php"); ?>
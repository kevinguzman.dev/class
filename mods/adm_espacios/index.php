<?php
  require_once("../required/header.php");
  require_once("espacio.php");

  $Gd_calendario    = new Espacio();
  $Gd_espacios      = json_encode($Gd_calendario->GetEspacios());
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Espacios
        <small>Todos</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Espacios</li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Listado de todos los espacios disponibles</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="espacios" class="table table-striped responsive table-hover">
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </thead>
            </table>
        </div>
    </div>
</section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(document).ready(function(){
    $("#espacios").DataTable({
        'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
        'paging'        : true,
        'lengthChange'  : true,
        'searching'     : true,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'responsive'    : true,
        'data'          : <?= $Gd_espacios ?>,
        'columns'       : [
                            { data: "id"},
                            { data: "nombre" },
                            { 
                                sortable: false,
                                "render": function(data, type, row, meta){
                                    var est = "";
                                    if(row.estado == "A"){
                                        est = "Activo";
                                        return "<a href='#'><span class='badge bg-green'>"+ est +"</span></a>";
                                    }else{
                                        est = "Inactivo";
                                        return "<a href='#'><span class='badge bg-red'>"+ est +"</span></a>";
                                    }

                                }
                            },
                            {
                                sortable: false,
                                className: "table-view-pf-actions",
                                "render": function (data, type, row, meta) {
                                    return "<a href='<?=$Gl_appUrl?>/adm_espacios/form/"+ row.id +"' class='btn btn-default' title='Ver horario'><i class='fa fa-calendar-minus-o'></i></a>";
                                }
                            },
                        ]
    });

});
</script>
<?php require_once("../required/scripts.php"); ?>
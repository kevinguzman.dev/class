<?php
require_once("../required/header.php");
require_once("gasto.php");
require_once("../periodos/periodo.php");

$Gd_gasto         = new Gasto();
$Gd_accion        = "ADD";
$Gd_codigo        = "";
$Gd_monto         = "";
$Gd_descripcion   = "";
$Gd_responsable   = "";
$Gd_periodo       = "";
$Gd_exito         = false;
$Gd_periodos      = $Gd_gasto->GetPeriodos();

if( isset($_POST["submit"]) ):
  $Gd_monto                 = $_POST["monto"];
  $Gd_descripcion           = $_POST["descripcion"];
  $Gd_codigo                = $_POST["codigo"];
  $Gd_periodo               = $_POST["periodo"];
  $Gd_responsable           = $_SESSION["Login"];

  if($Gd_codigo == ""):
    $Gd_codigo = 0;
  endif;

  $Gd_gasto                 = new Gasto();
  $Gd_gasto->codigo         = $Gd_codigo;
  $Gd_gasto->descripcion    = $Gd_descripcion;
  $Gd_gasto->monto          = limpiarNumero($Gd_monto);
  $Gd_gasto->responsable    = $Gd_responsable;
  $Gd_gasto->periodo        = $Gd_periodo;

  $Gd_gasto->Save();
  $Gd_codigo                = $Gd_gasto->codigo;
  $Gd_exito                 = true;
endif;

if(isset($_GET["id"]) and $_GET["id"] != "" or $Gd_accion == "GET"):
  $Gd_id            = $_GET["id"];
  $Gd_gasto         = new Gasto();
  $Gd_gasto         = $Gd_gasto->Get($Gd_id);

  $Gd_accion        = "GET";
  $Gd_codigo        = $Gd_gasto->codigo;
  $Gd_monto         = $Gd_gasto->monto;
  $Gd_descripcion   = $Gd_gasto->descripcion;
  $Gd_responsable   = $Gd_gasto->responsable;
  $Gd_periodo       = $Gd_gasto->periodo;
endif;
?>

<section class="content-header">
  <h1>
    <?php if($Gd_accion == "ADD"): ?>
      Registrar
    <?php else: ?>
      Modificar
    <?php endif; ?>
    Gasto
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/gastos/index">Gastos</a></li>
    <li class="active">
      <?php if($Gd_accion == "ADD"): ?>
        Registrar
      <?php else: ?>
        Modificar
      <?php endif; ?>
    </li>
  </ol>
</section>

<section class="content">
  <?php if($Gd_exito): ?>
  <div class="alert alert-success" id="msj">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Éxito:</strong> pago guardado con éxito ;-), haz <a href="<?= $Gl_appUrl ?>/gastos/index">haz click aquí</a> para volver al listado.
  </div>
  <?php endif; ?>

  <div class="row">
     <div class="col-xs-12">
       <div class="nav-tabs-custom">
         <ul class="nav nav-tabs">
           <li class="active"><a href="#gasto" data-toggle="tab">Gasto</a></li>
           <li class="dropdown pull-right">
             <a class="dropdown-toggle" data-toggle="dropdown" href="#">
               Opciones <span class="caret"></span>
             </a>
             <ul class="dropdown-menu">
               <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$Gl_appUrl?>/gastos/form">Registrar nuevo</a></li>
               <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$Gl_appUrl?>/gastos/index">Ver todos</a></li>
             </ul>
           </li>
         </ul>
         <div class="tab-content">
           <div class="tab-pane active" id="gasto">
             <form role="form" action="<?= $Gl_appUrl ?>/gastos/form/<?=$Gd_codigo?>" method="POST">
                <input type="hidden" name="codigo" value="<?= $Gd_codigo ?>">

                <div class="form-group">
                  <label for="periodo">Periodo</label>
                  <select class="form-control" name="periodo" id="periodo">
                    <?php for($i = 0; $i< count($Gd_periodos); $i++):
                        $Gd_select = "";
                        if($Gd_periodos[$i]->codigo == $Gd_periodo):
                          $Gd_select = "selected=''";
                        endif;
                      ?>
                      <option value="<?=$Gd_periodos[$i]->codigo ?>" <?= $Gd_select?>><?=$Gd_periodos[$i]->nombre ?> </option>
                      <?php endfor;?>
                  </select>
                </div>

                 <div class="form-group">
                   <label for="monto">Monto</label>
                   <div class="input-group">
                     <span class="input-group-addon"><b>$</b></span>
                     <input type="number" class="form-control" id="monto" name="monto" placeholder="Ingrese monto del gasto" value="<?= $Gd_monto?>" required="">
                   </div>
                 </div>

                 <div class="form-group">
                   <label for="observaciones">Observaciones</label>
                   <textarea class="form-control" name="descripcion" id="observaciones" rows="3" placeholder="Ingrese las observaciones del gasto, ya sea luz, agua, gas, etc...."><?= $Gd_descripcion ?></textarea>
                 </div>

                 <?php if($Gd_accion != "ADD"): ?>
                   <div class="form-group">
                     <label for="responsable">Responsable</label>
                     <input type="text" class="form-control" id="responsable" name="responsable" value="<?= $Gd_responsable ?>" disabled="">
                   </div>
                 <?php endif; ?>


               <div class="box-footer">
                 <button type="submit" class="btn btn-primary" name="submit">Guardar</button>
               </div>
             </form>
           </div>
         </div>

       </div>
     </div>
   </div>
</section>
<?php require_once("../required/footer.php"); ?>
<script type="text/javascript">
$('.select2').select2();
</script>
<?php require_once("../required/scripts.php"); ?>
